//
//  FiltersViewController.swift
//  ImageFramework
//
//  Created by Marius Avram on 4/12/19.
//

import UIKit

class FiltersViewController: UIViewController {

    @IBOutlet weak var imgMain: UIImageView!
    
    @IBOutlet var imgEffects: [UIImageView]!
    
    var currentImage:UIImage!
    
    var CIFilterNames = [
        "CIPhotoEffectChrome",
        "CIPhotoEffectFade",
        "CIPhotoEffectInstant",
        "CIPhotoEffectNoir",
        "CIPhotoEffectProcess",
        "CIPhotoEffectTonal",
        "CIPhotoEffectTransfer"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imgMain.image = ImageProcessor.sharedInstance.resultingImage
        imgMain.layer.cornerRadius = 4
        imgMain.clipsToBounds = true
        
        for img in imgEffects {
            img.layer.cornerRadius = 4
            img.clipsToBounds = true
            img.image = ImageProcessor.sharedInstance.resultingImage
            
            img.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(FiltersViewController.onEffect(sender:)))
            img.addGestureRecognizer(tap)
        }
        
        currentImage = ImageProcessor.sharedInstance.resultingImage
        
        let cimage = CIImage(image: ImageProcessor.sharedInstance.resultingImage!)
        imgEffects[1].image = sepiaFilter(cimage!, intensity: 0.9)
        imgEffects[2].image = bloomFilter(cimage!, intensity:1, radius:10)
        
        var index = 3
        for name in CIFilterNames {
            imgEffects[index].image = applyFilter(cimage!, name: name)
            index += 1
        }
        
        if self.navigationController != nil {
            let btnDone = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 36))
            btnDone.addTarget(self, action: #selector(CanvasViewController.onDone), for: .touchUpInside)
            btnDone.setTitle("Done", for: .normal)
            btnDone.setTitleColor(.blue, for: .normal)
            
            navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btnDone)
        }
    }

    @objc func onDone() {
        ImageProcessor.sharedInstance.resultingImage = imgMain.image
        self.dismiss(animated: true, completion: nil)
        //self.navigationController?.popToRootViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func sepiaFilter(_ input: CIImage, intensity: Double) -> UIImage
    {
        let ciContext = CIContext(options: nil)
        let sepiaFilter = CIFilter(name:"CISepiaTone")
        sepiaFilter?.setValue(input, forKey: kCIInputImageKey)
        sepiaFilter?.setValue(intensity, forKey: kCIInputIntensityKey)
        
        let filteredImageData = sepiaFilter!.value(forKey: kCIOutputImageKey) as! CIImage
        let filteredImageRef = ciContext.createCGImage(filteredImageData, from: filteredImageData.extent)
        
        return UIImage(cgImage: filteredImageRef!)
        //return sepiaFilter?.outputImage
    }
    
    func bloomFilter(_ input:CIImage, intensity: Double, radius: Double) -> UIImage
    {
        let ciContext = CIContext(options: nil)
        let bloomFilter = CIFilter(name:"CIBloom")
        bloomFilter?.setValue(input, forKey: kCIInputImageKey)
        bloomFilter?.setValue(intensity, forKey: kCIInputIntensityKey)
        bloomFilter?.setValue(radius, forKey: kCIInputRadiusKey)
        let filteredImageData = bloomFilter!.value(forKey: kCIOutputImageKey) as! CIImage
        let filteredImageRef = ciContext.createCGImage(filteredImageData, from: filteredImageData.extent)
        
        return UIImage(cgImage: filteredImageRef!)
    }
    
    func applyFilter(_ input:CIImage, name:String) -> UIImage {
        let ciContext = CIContext(options: nil)

        let filter = CIFilter(name: "\(name)" )
        filter!.setDefaults()
        filter!.setValue(input, forKey: kCIInputImageKey)
        let filteredImageData = filter!.value(forKey: kCIOutputImageKey) as! CIImage
        let filteredImageRef = ciContext.createCGImage(filteredImageData, from: filteredImageData.extent)
        
        return UIImage(cgImage: filteredImageRef!)
    }
    
    @objc func onEffect(sender:UITapGestureRecognizer) {
        if sender.view!.tag == 0 {
            imgMain.image = ImageProcessor.sharedInstance.resultingImage
        }

        imgMain.image = imgEffects[sender.view!.tag].image
    }
}
