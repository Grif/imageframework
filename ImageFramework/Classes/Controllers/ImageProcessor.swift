//
//  ImageProcessor.swift
//  ImageFramework
//
//  Created by Marius Avram on 4/2/19.
//

import UIKit

public enum ImageFormat:Int {
    case image169 = 0
    case imagePortrait
    case imageSquare
}

public class ImageProcessor: NSObject {
    @objc public static let sharedInstance = ImageProcessor()
    
    var backgroundImage:UIImage!
    var backgroundColor:UIColor!
    var overlays = [UIImageView]()
    var labels = [UILabel]()
    
    var assets = [String:[UIImage]]()
    
    public var resultingImage:UIImage!
    
    public var imageFormat:ImageFormat = .image169
    
    public func setAssets(name:String, images:[UIImage]) {
        assets[name] = images
    }
    
    public var CIFilterNames = [
        "CIPhotoEffectChrome",
        "CIPhotoEffectFade",
        "CIPhotoEffectInstant",
        "CIPhotoEffectNoir",
        "CIPhotoEffectProcess",
        "CIPhotoEffectTonal",
        "CIPhotoEffectTransfer"
    ]
    
    public func showCanvas(from:UIViewController) {
        let canvasViewController = CanvasViewController(nibName: "CanvasViewController", bundle: Bundle(for: CanvasViewController.self))
        let canvasNavController = UINavigationController(rootViewController: canvasViewController)
        
        from.present(canvasNavController, animated: true, completion: nil)
    }
    
    public func sepiaFilter(image:UIImage) -> UIImage {
        let cimage = CIImage(image: image)
        
        let ciContext = CIContext(options: nil)
        let sepiaFilter = CIFilter(name:"CISepiaTone")
        sepiaFilter?.setValue(cimage, forKey: kCIInputImageKey)
        sepiaFilter?.setValue(0.9, forKey: kCIInputIntensityKey)
        
        let filteredImageData = sepiaFilter!.value(forKey: kCIOutputImageKey) as! CIImage
        let filteredImageRef = ciContext.createCGImage(filteredImageData, from: filteredImageData.extent)
        
        return UIImage(cgImage: filteredImageRef!)
    }
    
    public func bloomFilter(image:UIImage) -> UIImage
    {
        let cimage = CIImage(image: image)
        
        let ciContext = CIContext(options: nil)
        let bloomFilter = CIFilter(name:"CIBloom")
        bloomFilter?.setValue(cimage, forKey: kCIInputImageKey)
        bloomFilter?.setValue(1, forKey: kCIInputIntensityKey)
        bloomFilter?.setValue(10, forKey: kCIInputRadiusKey)
        let filteredImageData = bloomFilter!.value(forKey: kCIOutputImageKey) as! CIImage
        let filteredImageRef = ciContext.createCGImage(filteredImageData, from: filteredImageData.extent)
        
        return UIImage(cgImage: filteredImageRef!)
    }
    
    public func applyFilter(_ image:UIImage, name:String) -> UIImage {
        
        let cimage = CIImage(image: image)
        
        let ciContext = CIContext(options: nil)
        
        let filter = CIFilter(name: "\(name)" )
        filter!.setDefaults()
        filter!.setValue(cimage, forKey: kCIInputImageKey)
        let filteredImageData = filter!.value(forKey: kCIOutputImageKey) as! CIImage
        let filteredImageRef = ciContext.createCGImage(filteredImageData, from: filteredImageData.extent)
        
        return UIImage(cgImage: filteredImageRef!)
    }
}
