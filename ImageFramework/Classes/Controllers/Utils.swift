//
//  Utils.swift
//  ImageFramework
//
//  Created by Marius Avram on 5/7/19.
//

import UIKit
import SwiftHSVColorPicker

protocol UtilsProtocol: NSObjectProtocol {
    func pickerColor(color:UIColor)
}

class Utils: NSObject {

    @objc public static let sharedInstance = Utils()
    
    weak var delegate:UtilsProtocol!
    
    var colorPicker:SwiftHSVColorPicker!
    
    func showColorPicker(parentController:UIViewController, color:UIColor? = nil) {
        let view = UIView(frame: parentController.view.bounds)
        view.tag = 1
        view.backgroundColor = .white
        colorPicker = SwiftHSVColorPicker(frame: CGRect(x: 20, y: 70, width: parentController.view.frame.width - 40, height: parentController.view.frame.height - 100))
        colorPicker.tag = 2
        view.addSubview(colorPicker)
        
        let btnClose = UIButton(frame: CGRect(x: 0, y: parentController.view.frame.height - 70, width: parentController.view.frame.width, height: 30))
        btnClose.setTitle("Choose", for: UIControlState.normal)
        btnClose.addTarget(self, action: #selector(Utils.closeColorPicker), for: .touchUpInside)
        btnClose.setTitleColor(.red, for: .normal)
        view.addSubview(btnClose)
        
        let btnCancel = UIButton(frame: CGRect(x: 0, y: parentController.view.frame.height - 30, width: parentController.view.frame.width, height: 30))
        btnCancel.setTitle("Cancel", for: UIControlState.normal)
        btnCancel.addTarget(self, action: #selector(Utils.cancelColorPicker), for: .touchUpInside)
        btnCancel.setTitleColor(.red, for: .normal)
        view.addSubview(btnCancel)
        
        parentController.view.addSubview(view)
        colorPicker.setViewColor(color != nil ? color! : .black)
    }
    
    @objc func closeColorPicker() {
        if delegate != nil {
            delegate!.pickerColor(color: colorPicker.color)
        }
        colorPicker.superview?.removeFromSuperview()
    }
    
    @objc func cancelColorPicker() {
        colorPicker.superview?.removeFromSuperview()
    }
}
