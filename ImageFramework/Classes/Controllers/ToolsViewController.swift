//
//  ToolsViewController.swift
//  ImageFramework
//
//  Created by Marius Avram on 4/2/19.
//

import UIKit
import Gallery
import Photos
import SwiftHSVColorPicker

class ToolsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var parentController:CanvasViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: "ToolMenuTableViewCell", bundle: Bundle(for: ToolsViewController.self)), forCellReuseIdentifier: "toolMenuTableViewCell")
    }


}

extension ToolsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "toolMenuTableViewCell") as! ToolMenuTableViewCell
        
        switch indexPath.row {
        case 0:
            cell.textLabel?.text = "Background Image"
        case 1:
            cell.textLabel?.text = "Background Color"
        case 2:
            cell.textLabel?.text = "Add Overlay"
        case 3:
            cell.textLabel?.text = "Add Text"
        case 4:
            cell.textLabel?.text = "Clear Overlays & Texts"
        default:
            break
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.setSelected(false, animated: true)
        }
        
        switch indexPath.row {
        case 0:
            let gallery = GalleryController()
            Config.Camera.recordLocation = true
            Config.tabsToShow = [.cameraTab, .imageTab]
            Config.Camera.imageLimit = 1
            gallery.delegate = self
            parentController.present(gallery, animated: true, completion: nil)
        case 1:
            parentController.onEdit()
            
            let view = UIView(frame: parentController.view.bounds)
            view.tag = 1
            view.backgroundColor = .white
            let colorPicker = SwiftHSVColorPicker(frame: CGRect(x: 20, y: 70, width: parentController.view.frame.width - 40, height: parentController.view.frame.height - 100))
            colorPicker.tag = 2
            view.addSubview(colorPicker)
            
            let btnClose = UIButton(frame: CGRect(x: 0, y: parentController.view.frame.height - 70, width: parentController.view.frame.width, height: 30))
            btnClose.setTitle("Choose", for: UIControlState.normal)
            btnClose.addTarget(self, action: #selector(ToolsViewController.closeColorPicker), for: .touchUpInside)
            btnClose.setTitleColor(.red, for: .normal)
            view.addSubview(btnClose)
            
            parentController.view.addSubview(view)
            colorPicker.setViewColor(UIColor.red)
            parentController.btnDone.isHidden = true
            parentController.btnNavRight.isHidden = true
        case 2:
            let overlayVC = OverlaysViewController(nibName: "OverlaysViewController", bundle: Bundle(for: OverlaysViewController.self))
            parentController.navigationController?.pushViewController(overlayVC, animated: true)
            break
        case 3:
            let labelVC = LabelViewController(nibName: "LabelViewController", bundle: Bundle(for: LabelViewController.self))
            parentController.navigationController?.pushViewController(labelVC, animated: true)
        case 4:
            ImageProcessor.sharedInstance.overlays.removeAll()
            ImageProcessor.sharedInstance.labels.removeAll()
            parentController.customize()
            parentController.onEdit()
        default:
            break
        }
    }
    
    @objc func closeColorPicker() {
        ImageProcessor.sharedInstance.backgroundImage = nil
        
        if let view = parentController.view.viewWithTag(1) {
            if let colorPicker = view.viewWithTag(2) as? SwiftHSVColorPicker {
                ImageProcessor.sharedInstance.backgroundColor = colorPicker.color
            }
            view.removeFromSuperview()
            parentController.customize()
        }
        parentController.btnDone.isHidden = false
        parentController.btnNavRight.isHidden = false
    }
}

extension ToolsViewController:GalleryControllerDelegate {
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        controller.dismiss(animated: true, completion: nil)
        if images.count > 0 {
            self.getThumbnail(assets: images) { (images) in
                ImageProcessor.sharedInstance.backgroundImage = images.first!
                //self.btnImage.setImage(images.first, for: .normal)
                //AudioVideoProcessor.sharedInstance.image = images.first!
            }
        }
    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        controller.dismiss(animated: true, completion: nil)
    }

    func getThumbnail(assets: [Image], completionHandler:(([UIImage]) -> Void)?) {
        var newImages = [UIImage]()
        var completedCount = 0
        for asset in assets {
            let manager = PHImageManager.default()
            
            let options = PHImageRequestOptions()
            
            options.version = .current
            options.isNetworkAccessAllowed = true
            options.isSynchronous = true
            
            manager.requestImageData(for: asset.asset, options: options) { data, _, _, _ in
                if let data = data {
                    if let thumbnail = UIImage(data: data) {
                        newImages.append(thumbnail)
                    }
                }
                
                completedCount += 1
                
                if completedCount >= assets.count {
                    if completionHandler != nil {
                        completionHandler!(newImages)
                    }
                }
            }
        }
    }
}
