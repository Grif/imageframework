//
//  LabelViewController.swift
//  ImageFramework
//
//  Created by Marius Avram on 5/7/19.
//

import UIKit

protocol LabelEditProtocol:NSObjectProtocol {
    func onLabelDelete()
}

class LabelViewController: UIViewController, UITextViewDelegate, UtilsProtocol {

    @IBOutlet weak var tvText: UITextView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var btnColorFill: UIButton!
    @IBOutlet weak var btnClone: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    @IBOutlet weak var fontSlider: UISlider!
    @IBOutlet weak var btnBold: UIButton!
    @IBOutlet weak var btnItalic: UIButton!
    @IBOutlet weak var btnUnderlined: UIButton!
    @IBOutlet weak var rotationSlider: UISlider!
    
    var editLabel:UILabel!
    
    weak var delegate:LabelEditProtocol!
    let style = NSMutableParagraphStyle()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        btnClose.layer.cornerRadius = 20
        btnClose.clipsToBounds = true
        
        btnColorFill.layer.cornerRadius = 20
        btnColorFill.clipsToBounds = true
        
        btnClone.layer.cornerRadius = 20
        btnClone.clipsToBounds = true
        
        btnDelete.layer.cornerRadius = 20
        btnDelete.clipsToBounds = true
        
        btnOk.layer.cornerRadius = 20
        btnOk.clipsToBounds = true
        
        tvText.delegate = self
        
        if editLabel != nil {
            tvText.text = editLabel.text
            tvText.textColor = editLabel.textColor
            tvText.font = editLabel.font
            fontSlider.value = Float(editLabel.font.pointSize)
        }
        else {
            btnClone.isHidden = true
            btnDelete.isHidden = true
        }
        
        btnItalic.titleLabel?.font = UIFont.italicSystemFont(ofSize: 21)
        
        style.alignment = NSTextAlignment.center
        
        tvText.textColor = editLabel != nil ? editLabel.textColor : .black
        
        let attr: [NSAttributedString.Key: Any] = [
            .font: UIFont.systemFont(ofSize: 21),
            .foregroundColor: tvText.textColor!,
            .underlineStyle: NSUnderlineStyle.styleSingle.rawValue,
            .paragraphStyle: style
        ]
        var attributeString = NSMutableAttributedString(string: "U",
                                                        attributes: attr)
        btnUnderlined.setAttributedTitle(attributeString, for: .normal)
        
        attributeString = NSMutableAttributedString(string: tvText.text,
                                                        attributes: attr)
        tvText.attributedText = attributeString
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if editLabel == nil {
            tvText.becomeFirstResponder()
        }
    }
    
    @IBAction func onOk(_ sender: Any) {
        let height = sizeOfString(string: tvText.text, constrainedTo: UIScreen.main.bounds.width - 20).height
        var label = UILabel(frame: CGRect(x: 10, y: 160, width: UIScreen.main.bounds.width - 20, height: height))
        if editLabel != nil {
            label = editLabel
            label.frame = CGRect(x: 10, y: 160, width: UIScreen.main.bounds.width - 20, height: height)
        }
        label.attributedText = tvText.attributedText
        label.textColor = tvText.textColor
        label.tag = 23
        label.numberOfLines = 0
        
        label.transform = CGAffineTransform(rotationAngle: CGFloat(deg2rad(Double(rotationSlider!.value))))
        
        if editLabel == nil {
            ImageProcessor.sharedInstance.labels.append(label)
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func deg2rad(_ number: Double) -> Double {
        return number * .pi / 180
    }
    
    @IBAction func onClose(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onColor(_ sender: Any) {
        tvText.resignFirstResponder()
        Utils.sharedInstance.showColorPicker(parentController: self, color: tvText.textColor)
        Utils.sharedInstance.delegate = self
    }
    
    func pickerColor(color: UIColor) {
        tvText.textColor = color
    }
    
    
    @IBAction func fontSizeChanged(_ sender: Any) {
        tvText.resignFirstResponder()
        tvText.font = UIFont.boldSystemFont(ofSize: CGFloat((sender as! UISlider).value))
    }
    
    @IBAction func onRotationChanged(_ sender: Any) {
    }
    
    @IBAction func onClone(_ sender: Any) {
        tvText.resignFirstResponder()
        let height = sizeOfString(string: tvText.text, constrainedTo: UIScreen.main.bounds.width - 20).height
        let label = UILabel(frame: CGRect(x: 10, y: 160, width: UIScreen.main.bounds.width - 20, height: height))
        label.attributedText = tvText.attributedText
        label.textColor = tvText.textColor
        label.tag = 23
        label.numberOfLines = 0
        label.transform = CGAffineTransform(rotationAngle: CGFloat(deg2rad(Double(rotationSlider!.value))))
        
        ImageProcessor.sharedInstance.labels.append(label)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func sizeOfString (string: String, constrainedTo width: CGFloat) -> CGSize {
        return NSString(string: string).boundingRect(with: CGSize(width:width, height: CGFloat(Double.greatestFiniteMagnitude)),
                                                     options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                     attributes: [NSAttributedStringKey.font: tvText.font!],
                                                     context: nil).size
    }
    
    @IBAction func onDelete(_ sender: Any) {
        tvText.resignFirstResponder()
        if delegate != nil {
            delegate.onLabelDelete()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onBold(_ sender: Any) {
        if !btnBold.isSelected {
            let attr: [NSAttributedString.Key: Any] = [
                .font: UIFont.boldSystemFont(ofSize: CGFloat(fontSlider!.value)),
                .foregroundColor: tvText.textColor!,
                .underlineStyle: btnUnderlined.isSelected ? NSUnderlineStyle.styleSingle.rawValue : NSUnderlineStyle.styleNone.rawValue,
                .paragraphStyle: style]
            let attributeString = NSMutableAttributedString(string: tvText.text,
                                                            attributes: attr)
            tvText.attributedText = attributeString
            btnItalic.isSelected = false
        }
        else {
            let attr: [NSAttributedString.Key: Any] = [
                .font: UIFont.systemFont(ofSize: CGFloat(fontSlider!.value)),
                .foregroundColor: tvText.textColor!,
                .underlineStyle: btnUnderlined.isSelected ? NSUnderlineStyle.styleSingle.rawValue : NSUnderlineStyle.styleNone.rawValue,
                .paragraphStyle: style]
            let attributeString = NSMutableAttributedString(string: tvText.text,
                                                            attributes: attr)
            tvText.attributedText = attributeString
        }
        btnBold.isSelected = !btnBold.isSelected
    }
    
    @IBAction func onItalic(_ sender: Any) {
        if !btnItalic.isSelected {
            btnBold.isSelected = false
            let attr: [NSAttributedString.Key: Any] = [
                .font: UIFont.italicSystemFont(ofSize: CGFloat(fontSlider!.value)),
                .foregroundColor: tvText.textColor!,
                .underlineStyle: btnUnderlined.isSelected ? NSUnderlineStyle.styleSingle.rawValue : NSUnderlineStyle.styleNone.rawValue,
                .paragraphStyle: style]
            let attributeString = NSMutableAttributedString(string: tvText.text,
                                                            attributes: attr)
            tvText.attributedText = attributeString
        }
        else {
            let attr: [NSAttributedString.Key: Any] = [
                .font: UIFont.systemFont(ofSize: CGFloat(fontSlider!.value)),
                .foregroundColor: tvText.textColor!,
                .underlineStyle: btnUnderlined.isSelected ? NSUnderlineStyle.styleSingle.rawValue : NSUnderlineStyle.styleNone.rawValue,
                .paragraphStyle: style]
            let attributeString = NSMutableAttributedString(string: tvText.text,
                                                            attributes: attr)
            tvText.attributedText = attributeString
        }
        btnItalic.isSelected = !btnItalic.isSelected
    }
    
    @IBAction func onUnderlined(_ sender: Any) {
        if !btnUnderlined.isSelected {
            let attr: [NSAttributedString.Key: Any] = [
                .font: btnBold.isSelected ? UIFont.boldSystemFont(ofSize: CGFloat(fontSlider!.value)) : (btnItalic.isSelected ? UIFont.italicSystemFont(ofSize: CGFloat(fontSlider!.value)) : UIFont.systemFont(ofSize: CGFloat(fontSlider!.value))),
                .foregroundColor: tvText.textColor!,
                .underlineStyle: NSUnderlineStyle.styleSingle.rawValue,
                .paragraphStyle: style]
            let attributeString = NSMutableAttributedString(string: tvText.text,
                                                            attributes: attr)
            tvText.attributedText = attributeString
        }
        else {
            let attr: [NSAttributedString.Key: Any] = [
                .font: btnBold.isSelected ? UIFont.boldSystemFont(ofSize: CGFloat(fontSlider!.value)) : (btnItalic.isSelected ? UIFont.italicSystemFont(ofSize: CGFloat(fontSlider!.value)) : UIFont.systemFont(ofSize: CGFloat(fontSlider!.value))),
                .foregroundColor: tvText.textColor!,
                .underlineStyle: NSUnderlineStyle.styleNone.rawValue,
                .paragraphStyle: style]
            let attributeString = NSMutableAttributedString(string: tvText.text,
                                                            attributes: attr)
            tvText.attributedText = attributeString
        }
        btnUnderlined.isSelected = !btnUnderlined.isSelected
    }
}
