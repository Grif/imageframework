# ImageFramework

[![CI Status](https://img.shields.io/travis/Marius Avram/ImageFramework.svg?style=flat)](https://travis-ci.org/Marius Avram/ImageFramework)
[![Version](https://img.shields.io/cocoapods/v/ImageFramework.svg?style=flat)](https://cocoapods.org/pods/ImageFramework)
[![License](https://img.shields.io/cocoapods/l/ImageFramework.svg?style=flat)](https://cocoapods.org/pods/ImageFramework)
[![Platform](https://img.shields.io/cocoapods/p/ImageFramework.svg?style=flat)](https://cocoapods.org/pods/ImageFramework)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ImageFramework is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ImageFramework'
```

## iCloud
To setup iCloud file upload, the following actions are required:

- go to developer.apple.com in the Certificates, Identifiers & Profiles section, find the iCloud Containers and define a new container for your App bundle id

- go to the App IDs section, find your App Id, choose edit and enable the iCloud service

- then choose edit for the iCloud service, select the container created above

- update the provisioning profile for development and release 



## Author

Marius Avram, marius@codapper.com

## License

ImageFramework is available under the MIT license. See the LICENSE file for more info.
