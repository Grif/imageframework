//
//  OverlayImageEditViewController.swift
//  ImageFramework
//
//  Created by Marius Avram on 4/22/19.
//

import UIKit

protocol OverlayImageEditProtocol:NSObjectProtocol {
    func onEditDone(image:UIImage)
    func onEditDelete()
}

extension UIImage {
    func rotate(radians: CGFloat) -> UIImage {
        let rotatedSize = CGRect(origin: .zero, size: size)
            .applying(CGAffineTransform(rotationAngle: CGFloat(radians)))
            .integral.size
        UIGraphicsBeginImageContext(rotatedSize)
        if let context = UIGraphicsGetCurrentContext() {
            let origin = CGPoint(x: rotatedSize.width / 2.0,
                                 y: rotatedSize.height / 2.0)
            context.translateBy(x: origin.x, y: origin.y)
            context.rotate(by: radians)
            draw(in: CGRect(x: -origin.x, y: -origin.y,
                            width: size.width, height: size.height))
            let rotatedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return rotatedImage ?? self
        }
        
        return self
    }
}

extension UIImage {
    public func imageRotatedByDegrees(degrees: CGFloat, flip: Bool) -> UIImage {
        let radiansToDegrees: (CGFloat) -> CGFloat = {
            return $0 * (180.0 / CGFloat(M_PI))
        }
        let degreesToRadians: (CGFloat) -> CGFloat = {
            return $0 / 180.0 * CGFloat(M_PI)
        }
        
        // calculate the size of the rotated view's containing box for our drawing space
        let rotatedViewBox = UIView(frame: CGRect(origin: CGPoint.zero, size: size))
        let t = CGAffineTransform(rotationAngle: degreesToRadians(degrees));
        rotatedViewBox.transform = t
        let rotatedSize = rotatedViewBox.frame.size
        
        // Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize)
        let bitmap = UIGraphicsGetCurrentContext()!
        
        // Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap.translateBy(x: rotatedSize.width / 2.0, y: rotatedSize.height / 2.0)
        
        //   // Rotate the image context
        bitmap.rotate(by: degreesToRadians(degrees))
        
        // Now, draw the rotated/scaled image into the context
        var yFlip: CGFloat
        
        if(flip){
            yFlip = CGFloat(-1.0)
        } else {
            yFlip = CGFloat(1.0)
        }
        
        bitmap.scaleBy(x: yFlip, y:  -1.0)
        bitmap.draw(self.cgImage!, in: CGRect(x: -size.width / 2, y: -size.height / 2, width: size.width, height: size.height))
        
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

extension UIImage {
    func imageWithColor(color1: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color1.setFill()
        
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(origin: .zero, size: CGSize(width: self.size.width, height: self.size.height))
        context?.clip(to: rect, mask: self.cgImage!)
        context?.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}


class OverlayImageEditViewController: UIViewController, UtilsProtocol {
    
    @IBOutlet weak var previewImage: UIImageView!
    
    var initialImage:UIImage!
    
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnClone: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var btnColorFill: UIButton!
    
    weak var delegate:OverlayImageEditProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnDelete.layer.cornerRadius = 8
        btnDelete.clipsToBounds = true
        
        btnClone.layer.cornerRadius = 8
        btnClone.clipsToBounds = true
        
        btnClose.layer.cornerRadius = 20
        btnClose.clipsToBounds = true
        
        btnColorFill.layer.cornerRadius = 20
        btnColorFill.clipsToBounds = true
        
        btnOk.layer.cornerRadius = 20
        btnOk.clipsToBounds = true
        
        initialImage = previewImage.image
        previewImage.contentMode = .scaleAspectFill
    }

    @IBAction func sliderChanged(_ sender: Any) {
        if initialImage == nil {
            initialImage = previewImage.image
        }
        previewImage.image = initialImage.imageRotatedByDegrees(degrees: CGFloat((sender as! UISlider).value), flip: false)//rotate(radians: CGFloat(deg2rad(Double((sender as! UISlider).value))))// (CGFloat((sender as! UISlider).value) * CGFloat(Double.pi / 180))) //imageRotatedByDegrees(oldImage: initialImage, deg: CGFloat((sender as! UISlider).value))
    }
    
    func imageRotatedByDegrees(oldImage: UIImage, deg degrees: CGFloat) -> UIImage {
        let size = oldImage.size
        
        UIGraphicsBeginImageContext(size)
        
        let bitmap: CGContext = UIGraphicsGetCurrentContext()!
        //Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap.translateBy(x: size.width / 2, y: size.height / 2)
        //Rotate the image context
        bitmap.rotate(by: (degrees * CGFloat(Double.pi / 180)))
        //Now, draw the rotated/scaled image into the context
        bitmap.scaleBy(x: 1.0, y: -1.0)
        
        let origin = CGPoint(x: -size.width / 2, y: -size.width / 2)
        
        bitmap.draw(oldImage.cgImage!, in: CGRect(origin: origin, size: size))
        
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }

    @IBAction func onClone(_ sender: Any) {
        let imgView = UIImageView(frame: CGRect(x: 10, y: 160, width: 100, height: 100))
        imgView.image = previewImage.image
        imgView.contentMode = .scaleAspectFit
        
        imgView.tag = 22
        ImageProcessor.sharedInstance.overlays.append(imgView)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onColorFill(_ sender: Any) {
        Utils.sharedInstance.showColorPicker(parentController: self)
        Utils.sharedInstance.delegate = self
    }
    
    func pickerColor(color: UIColor) {
        previewImage.image = previewImage.image?.imageWithColor(color1: color)
    }
    
    @IBAction func onDelete(_ sender: Any) {
        if delegate != nil {
            delegate.onEditDelete()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onOk(_ sender: Any) {
        if delegate != nil {
            delegate.onEditDone(image: previewImage.image!)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func deg2rad(_ number: Double) -> Double {
        return number * .pi / 180
    }
}
