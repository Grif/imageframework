//
//  ViewController.swift
//  ShareFramework
//
//  Created by Marius Avram on 01/11/2019.
//  Copyright (c) 2019 Marius Avram. All rights reserved.
//

import UIKit
import ImageFramework

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        ImageProcessor.sharedInstance.setAssets(name: "Icons", images: [UIImage(named: "1")!])
        ImageProcessor.sharedInstance.setAssets(name: "Abstract", images: [UIImage(named: "2")!, UIImage(named: "3")!, UIImage(named: "4")!])
        ImageProcessor.sharedInstance.setAssets(name: "Characters", images: [UIImage(named: "5")!, UIImage(named: "6")!])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    //MARK: TableView data source & delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "defaultCell")
        cell?.detailTextLabel?.text = "start creating your custom image"
        cell?.textLabel?.text = "Canvas"
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.setSelected(false, animated: true)
        }
        
        ImageProcessor.sharedInstance.showCanvas(from:self)
        //let canvasViewController = CanvasViewController(nibName: "CanvasViewController", bundle: Bundle(for: CanvasViewController.self))
        //self.navigationController?.pushViewController(canvasViewController, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
    
    //MARK: helper
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
}

