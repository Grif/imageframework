//
//  OverlaysViewController.swift
//  ImageFramework
//
//  Created by Marius Avram on 4/3/19.
//

import UIKit

class OverlaysViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup ofter loading the view.
        
        collectionView.register(UINib(nibName: "OverlayCollectionViewCell", bundle: Bundle(for: OverlaysViewController.self)), forCellWithReuseIdentifier: "overlayCollectionViewCell")
        collectionView.register(UINib(nibName: "OverlayCollectionReusableView", bundle: Bundle(for: OverlayCollectionReusableView.self)), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "overlayCollectionReusableView")
        
        if self.navigationController != nil {
            let btnDone = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 36))
            btnDone.addTarget(self, action: #selector(OverlaysViewController.onDone), for: .touchUpInside)
            btnDone.setTitle("Done", for: .normal)
            btnDone.setTitleColor(.blue, for: .normal)
            
            navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btnDone)
        }
    }

    @objc func onDone() {
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension OverlaysViewController:UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return ImageProcessor.sharedInstance.assets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let index = ImageProcessor.sharedInstance.assets.keys.index(ImageProcessor.sharedInstance.assets.keys.startIndex, offsetBy: section)
        return ImageProcessor.sharedInstance.assets[index].value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let size = (collectionView.frame.size.width - 30) / 3
        return CGSize(width: size, height: size)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "overlayCollectionViewCell", for: indexPath)
        if let imgView = cell.contentView.viewWithTag(1) as? UIImageView {
            imgView.layer.cornerRadius = 12
            imgView.clipsToBounds = true
            imgView.layer.borderColor = UIColor.gray.cgColor
            imgView.layer.borderWidth = 1

            let index = ImageProcessor.sharedInstance.assets.keys.index(ImageProcessor.sharedInstance.assets.keys.startIndex, offsetBy: indexPath.section)
            imgView.image = ImageProcessor.sharedInstance.assets[index].value[indexPath.row]
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = ImageProcessor.sharedInstance.assets.keys.index(ImageProcessor.sharedInstance.assets.keys.startIndex, offsetBy: indexPath.section)
        let imgView = UIImageView(frame: CGRect(x: 10, y: 160, width: 100, height: 100))
        imgView.image = ImageProcessor.sharedInstance.assets[index].value[indexPath.row]
        imgView.contentMode = .scaleAspectFit
        
        imgView.tag = 22
        ImageProcessor.sharedInstance.overlays.append(imgView)
        self.navigationController?.popViewController(animated: true)
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if (kind == UICollectionElementKindSectionFooter) {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "footer", for: indexPath)

            // Customize footerView here
            return footerView
        } else if (kind == UICollectionElementKindSectionHeader) {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "overlayCollectionReusableView", for: indexPath)
            // Customize headerView here
            if let title = headerView.viewWithTag(1) as? UILabel {
                let index = ImageProcessor.sharedInstance.assets.keys.index(ImageProcessor.sharedInstance.assets.keys.startIndex, offsetBy: indexPath.section)
                title.text = ImageProcessor.sharedInstance.assets[index].key
            }
            return headerView
        }
        fatalError()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let size = CGSize(width: 400, height: 60)
        return size
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
//        let size = CGSize(width: 400, height: 50)
//        return size
//    }
}
