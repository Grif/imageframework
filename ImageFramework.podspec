#
# Be sure to run `pod lib lint ImageFramework.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ImageFramework'
  s.version          = '0.1.167'
  s.summary          = 'ImageFramework - social networking sharing, drive exports, file transfer.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/Grif/Imageframework/src/master/'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Marius Avram' => 'marius@codapper.com' }
  s.source           = { :git => 'https://Grif@bitbucket.org/Grif/Imageframework.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'

  s.source_files = 'ImageFramework/Classes/**/*'#, 'ImageFramework/Frameworks/LoginWithAmazon.framework/Headers/*.h', 'ImageFramework/Frameworks/ACDSKit.framework/Headers/*.h'
  s.swift_version = '4.0'
  # s.resource_bundles = {
  #   'ImageFramework' => ['ImageFramework/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.ios.deployment_target = '10.0'
  s.watchos.deployment_target = '3.2'
  s.tvos.deployment_target = '10.13'
  s.osx.deployment_target  = '10.12'
  s.frameworks = 'CoreFoundation'
  #s.vendored_frameworks = 'ImageFramework/Frameworks/LoginWithAmazon.framework', 'ImageFramework/Frameworks/ACDSKit.framework'
  
  s.dependency 'Gallery'
  s.dependency 'SwiftHSVColorPicker'
  
  #s.ios.public_header_files = 'ImageFramework/Frameworks/LoginWithAmazon.framework/Headers/*.h', 'ImageFramework/Frameworks/ACDSKit.framework/Headers/*.h'
  
end
