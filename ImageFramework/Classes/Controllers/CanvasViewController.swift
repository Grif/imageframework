//
//  CanvasViewController.swift
//  ImageFramework_Example
//
//  Created by Marius Avram on 4/2/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import SwiftHSVColorPicker

extension UIView {
    func asImage() -> UIImage {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(bounds: bounds)
            return renderer.image { rendererContext in
                layer.render(in: rendererContext.cgContext)
            }
        } else {
            UIGraphicsBeginImageContext(self.frame.size)
            self.layer.render(in:UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return UIImage(cgImage: image!.cgImage!)
        }
    }
}

open class CanvasViewController: UIViewController, OverlayImageEditProtocol, LabelEditProtocol {
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var backgroundView: UIView!
    var toolsViewController = ToolsViewController(nibName: "ToolsViewController", bundle: Bundle(for: ToolsViewController.self))
    var btnNavRight:UIButton!
    var btnDone:UIButton!
    
    var selectedButton:UIView!
    var movingImage: UIImageView!
    var originalCenter = CGPoint()
    
    var lastScale:CGFloat = 1
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    override open func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if self.navigationController != nil {
            btnNavRight = UIButton(frame: CGRect(x: 0, y: 0, width: 70, height: 36))
            btnNavRight.addTarget(self, action: #selector(CanvasViewController.onEdit), for: .touchUpInside)
            btnNavRight.setTitle("Tools", for: .normal)
            btnNavRight.setTitleColor(.blue, for: .normal)
            
            btnDone = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 36))
            btnDone.addTarget(self, action: #selector(CanvasViewController.onDone), for: .touchUpInside)
            btnDone.setTitle("Done", for: .normal)
            btnDone.setTitleColor(.blue, for: .normal)
            
            navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: btnNavRight), UIBarButtonItem(customView: btnDone)]
        }
        self.title = "Canvas"
        self.view.addSubview(toolsViewController.view)
        toolsViewController.parentController = self
        toolsViewController.view.frame =  CGRect(x: self.view.bounds.width, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        
        backgroundImage.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(CanvasViewController.onTapBackground))
        backgroundImage.addGestureRecognizer(tap)
        
        backgroundImage.clipsToBounds = true
        
        switch ImageProcessor.sharedInstance.imageFormat {
        case .image169:
            let height = (9 * UIScreen.main.bounds.width) / 16
            let heighView = UIScreen.main.bounds.height - 64
            topConstraint.constant = (heighView - height) / 2
            bottomConstraint.constant = (heighView - height) / 2
        case .imagePortrait:
            break
        case .imageSquare:
            let heighView = UIScreen.main.bounds.height - 64
            topConstraint.constant = (heighView - UIScreen.main.bounds.width) / 2
            bottomConstraint.constant = (heighView - UIScreen.main.bounds.width) / 2
        }
    }

    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        toolsViewController.view.frame =  CGRect(x: self.view.bounds.width, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        if selectedButton == nil {
            self.btnNavRight.setTitle("Tools", for: .normal)
        }
        else {
            self.btnNavRight.setTitle("Edit", for: .normal)
        }
        
        customize()
        btnDone.isHidden = false
        btnNavRight.isHidden = false
    }
    
    func customize() {
        while let imgView = self.view.viewWithTag(22) {
            if imgView.superview == backgroundView {
                imgView.frame.origin = self.backgroundView.convert(imgView.frame.origin, to: self.view)
                imgView.removeFromSuperview()
            }
            imgView.removeFromSuperview()
        }
        for imgView in ImageProcessor.sharedInstance.overlays {
            addPanGestureToButton(imgView)
            addPinchGesture(imgView)
            addTapGesture(imgView)
            self.view.insertSubview(imgView, belowSubview: toolsViewController.view)
        }
        
        while let imgView = self.view.viewWithTag(23) {
            if imgView.superview == backgroundView {
                imgView.frame.origin = self.backgroundView.convert(imgView.frame.origin, to: self.view)
                imgView.removeFromSuperview()
            }
            imgView.removeFromSuperview()
        }
        for lbl in ImageProcessor.sharedInstance.labels {
            addPanGestureToButton(lbl)
            addTapGesture(lbl)
            self.view.insertSubview(lbl, belowSubview: toolsViewController.view)
        }
        backgroundImage.image = ImageProcessor.sharedInstance.backgroundImage
        if ImageProcessor.sharedInstance.backgroundColor != nil {
            backgroundImage.backgroundColor = ImageProcessor.sharedInstance.backgroundColor
        }
    }

    @objc func onEdit() {
        if selectedButton == nil {
            UIView.animate(withDuration: 0.3, animations: {
                if self.toolsViewController.view.frame.origin.x != 40 {
                    self.btnNavRight.setTitle("Close", for: .normal)
                    self.toolsViewController.view.frame = CGRect(x: 40, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
                }
                else {
                    self.btnNavRight.setTitle("Tools", for: .normal)
                    self.toolsViewController.view.frame = CGRect(x: self.view.bounds.width, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
                }
            }) { (completed) in
                
            }
        }
        else {
            if let img = selectedButton as? UIImageView {
                let controller = OverlayImageEditViewController(nibName: "OverlayImageEditViewController", bundle: Bundle(for: OverlayImageEditViewController.self))
                self.present(controller, animated: true, completion: nil)
                controller.previewImage.image = (selectedButton as! UIImageView).image
                controller.delegate = self
            }
            else {
                let controller = LabelViewController(nibName: "LabelViewController", bundle: Bundle(for: LabelViewController.self))
                controller.editLabel = selectedButton as! UILabel
                self.navigationController?.pushViewController(controller, animated: true)
                
                controller.delegate = self
            }
        }
    }
    
    func onEditDone(image: UIImage) {
        (selectedButton as! UIImageView).image = image
    }
    
    func onEditDelete() {
        if let index = ImageProcessor.sharedInstance.overlays.index(of: selectedButton as! UIImageView) {
            selectedButton.removeFromSuperview()
            ImageProcessor.sharedInstance.overlays.remove(at: index)
        }
        unselectView()
    }
    
    func onLabelDelete() {
        if let index = ImageProcessor.sharedInstance.labels.index(of: selectedButton as! UILabel) {
            selectedButton.removeFromSuperview()
            ImageProcessor.sharedInstance.labels.remove(at: index)
        }
        unselectView()
    }
    
    @objc func onDone() {
        unselectView()
        if let colorPick = view.viewWithTag(1) {
            if let colorPicker = colorPick.viewWithTag(2) as? SwiftHSVColorPicker {
                ImageProcessor.sharedInstance.backgroundColor = colorPicker.color
            }
            colorPick.removeFromSuperview()
            self.customize()
        }
        else {
            btnDone.isHidden = true
            btnNavRight.isHidden = true
            Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(CanvasViewController.showFilters), userInfo: nil, repeats: false)
        }
    }
    
    @objc func showFilters() {
        for imgView in ImageProcessor.sharedInstance.overlays {
            imgView.frame.origin = self.view.convert(imgView.frame.origin, to: backgroundView)
            imgView.removeFromSuperview()
            backgroundView.addSubview(imgView)
        }
        
        for lbl in ImageProcessor.sharedInstance.labels {
            lbl.frame.origin = self.view.convert(lbl.frame.origin, to: backgroundView)
            lbl.removeFromSuperview()
            backgroundView.addSubview(lbl)
        }
        
        ImageProcessor.sharedInstance.resultingImage = self.backgroundView.asImage()

        let filters = FiltersViewController(nibName: "FiltersViewController", bundle: Bundle(for: FiltersViewController.self))
        self.navigationController?.pushViewController(filters, animated: true)
    }
    
    @objc func onTapBackground() {
        unselectView()
    }
    
    func selectView(_ view:UIView) {
        unselectView()
        selectedButton = view
        
        selectedButton.layer.borderColor = UIColor.blue.cgColor
        selectedButton.layer.borderWidth = 2
        
        btnNavRight.setTitle("Edit", for: .normal)
    }
    func unselectView() {
        if selectedButton != nil {
            selectedButton.layer.borderWidth = 0
        }
        selectedButton = nil
        btnNavRight.setTitle("Tools", for: .normal)
    }
    
    func addTapGesture(_ view: UIView) {
        let panRec = UITapGestureRecognizer()
        
        panRec.addTarget(self, action: #selector(CanvasViewController.onTapOverlay(_:)))
        view.addGestureRecognizer(panRec)
        view.isUserInteractionEnabled = true
    }
    
    @objc func onTapOverlay(_ gesture: UITapGestureRecognizer) {
        selectView(gesture.view!)
    }

    //MARK: reorder images
    func addPanGestureToButton(_ button: UIView) {
        let panRec = UIPanGestureRecognizer()
        panRec.minimumNumberOfTouches = 1
        panRec.delaysTouchesBegan = true
        
        panRec.addTarget(self, action: #selector(CanvasViewController.draggedView(_:)))
        button.addGestureRecognizer(panRec)
        button.isUserInteractionEnabled = true
    }
    
    
    @objc func draggedView(_ sender:UIPanGestureRecognizer){
        selectView(sender.view!)
        if(sender.state == UIGestureRecognizer.State.began )
        {
            self.view.bringSubview(toFront: selectedButton)
            originalCenter = (sender.view?.center)!
            if let img = selectedButton as? UIImageView {
                movingImage = img
            }
            else {
                movingImage = UIImageView(image: selectedButton.asImage())
                movingImage.center = selectedButton.center
                self.view.addSubview(movingImage)
                selectedButton.isHidden = true
            }
        }
        
        let translation = sender.translation(in: movingImage.superview)
        
        let tmp = movingImage.center.x
        let tmp1 = movingImage.center.y
        
        movingImage.center = CGPoint(x:tmp+translation.x, y:tmp1+translation.y)
        sender.setTranslation(CGPoint(x:0, y:0), in: movingImage.superview)
        
        if(sender.state == UIGestureRecognizer.State.ended || sender.state == UIGestureRecognizer.State.failed || sender.state == UIGestureRecognizer.State.cancelled)
        {
            self.view.bringSubview(toFront: toolsViewController.view)
            if movingImage != selectedButton {
                movingImage.removeFromSuperview()
                selectedButton.center = movingImage.center
            }
            selectedButton.isHidden = false
        }
    }
    
    //MARK: pinch to zoom
    func addPinchGesture(_ view: UIImageView) {
        let panRec = UIPinchGestureRecognizer()
        //panRec.minimumNumberOfTouches = 1
        panRec.delaysTouchesBegan = true
        
        panRec.addTarget(self, action: #selector(CanvasViewController.zoom(gesture:)))
        view.addGestureRecognizer(panRec)
        view.isUserInteractionEnabled = true
    }
    
    @objc func zoom(gesture:UIPinchGestureRecognizer) {
        selectView(gesture.view!)
        
        if(gesture.state == .began) {
            // Reset the last scale, necessary if there are multiple objects with different scales
            lastScale = gesture.scale
        }
        if (gesture.state == .began || gesture.state == .changed) {
            let currentScale = gesture.view!.layer.value(forKeyPath:"transform.scale")! as! CGFloat
            // Constants to adjust the max/min values of zoom
            let kMaxScale:CGFloat = 2.0
            let kMinScale:CGFloat = 1.0
            var newScale = 1 -  (lastScale - gesture.scale)
            newScale = min(newScale, kMaxScale / currentScale)
            newScale = max(newScale, kMinScale / currentScale)
            let transform = (gesture.view?.transform)!.scaledBy(x: newScale, y: newScale);
            gesture.view?.transform = transform
            lastScale = gesture.scale  // Store the previous scale factor for the next pinch gesture call
        }
    }
}
